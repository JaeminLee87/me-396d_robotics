#########################################################################################
## READ ME By JM & MK 
## This code includes both system validation and model predictive control results.
## If you don't want to seperate two parts, please make sure system coefficients. 
## Because this code is sharing system specification and model, 
## you should re-defin system coefficient in case of modifying any part of this.

import numpy as np
import matplotlib.pyplot as plt
from cvxpy import *

vtau1, vtau2, vtau3, vtau4, vtau5, vtau6 = 2, 2, 2, 2, 2, 3
vgamma11, vgamma22, vgamma33, vgamma44 = 2, 3, 3, 2
# vbeta62, vbeta32, vbeta63, vbeta14, vbeta24, vbeta15, vbeta25, vbeta35, vbeta46, vbeta56 = -0.2, 0.15, 0.3, 0.4, -0.35, -0.2, 0.15, -0.2, 0.36, 0.25

vbeta62 = 0.3
vbeta12 = -0.2
vbeta23 = 0.2
vbeta24 = -0.1
vbeta25 = 0.25
vbeta45 = -0.1
vbeta36 = 0.25
vbeta56 = 0.25

# define the number of data (days)
N_data = 50
time = range(N_data)

# define delta t (1day)
unit_tau = 1

# define all zeta are zero
no_disturb = 0

## start Model Predictive Controller
## We are using cvxpy, thus, please make sure to install cvxpy.

# define state variable & input & output
# A = np.zeros((6,6),float)
# B = np.zeros((6,3),float)
# C = np.eye(6)

# # Transformation of system description in form of Matrix
# A[0,0] = 1 - 1/float(vtau1)
# A[1,1] = 1 - 1/float(vtau2)
# A[1,2] = float(vbeta32)/float(vtau2)
# A[1,5] = float(vbeta62)/float(vtau2)
# A[2,2] = 1 - 1/float(vtau3)
# A[2,5] = float(vbeta63)/float(vtau3)
# A[3,0] = float(vbeta14)/float(vtau4) 
# A[3,1] = float(vbeta24)/float(vtau4)
# A[3,3] = 1 - 1/float(vtau4)
# A[4,0] = float(vbeta15)/float(vtau5)
# A[4,1] = float(vbeta25)/float(vtau5)
# A[4,2] = float(vbeta35)/float(vtau5)
# A[4,4] = 1 - 1/float(vtau5)
# A[5,3] = float(vbeta46)/float(vtau6)
# A[5,4] = float(vbeta56)/float(vtau6)
# A[5,5] = 1 - 1/float(vtau6)

# B[0,0] = float(vgamma11)/float(vtau1)
# B[1,1] = float(vgamma22)/float(vtau2)
# B[2,2] = float(vgamma33)/float(vtau3)

A = np.zeros((6,6),float)
B = np.zeros((6,4),float)

A[0,0] = -1/float(vtau1)

A[1,1] = -1/float(vtau2)
A[1,5] = float(vbeta62)/float(vtau2)

A[2,0] = float(vbeta12)/float(vtau3)
A[2,1] = float(vbeta23)/float(vtau3)
A[2,2] = -1/float(vtau3)

A[3,1] = float(vbeta24)/float(vtau4)
A[3,3] = -1/float(vtau4)

A[4,1] = float(vbeta25)/float(vtau5)
A[4,3] = float(vbeta45)/float(vtau5)
A[4,4] = -1/float(vtau5)

A[5,2] = float(vbeta36)/float(vtau6)
A[5,4] = float(vbeta56)/float(vtau6)
A[5,5] = -1/float(vtau6)

B[0,0] = float(vgamma11)/float(vtau1)
B[0,1] = float(vgamma22)/float(vtau1)
B[1,1] = float(vgamma22)/float(vtau2)
B[1,2] = float(vgamma33)/float(vtau2)
B[2,0] = float(vgamma11)/float(vtau3)
B[2,3] = float(vgamma44)/float(vtau3)


#x = Variable(6,N_data)
x_res = np.zeros((6,N_data),float)		# state results
x_des = np.array([0 ,0, 0, 0, 0, 10])	# desired state 

# constriants for states
x_min = np.array([0, -100, 0, 0, -100, 0])
x_max = np.array([100, 100, 100, 100, 50, 100])

u_res = np.zeros((4,N_data),float)		# input results
u_des = np.zeros((4, N_data+1), float)	# desired input 

# constriants for inputs
u_min = np.array([-100, -100, -100, -100])			
u_max = np.array([200, 200, 200, 200])
du_min = np.array([-1,-1,-1,-1])
du_max = np.array([1, 1, 1,1])

# horizon steps for prediction and control
P_horizon = 5 			# horizon for predection
C_horizon = 3 			# horizon for control

# weighting matrix for cost fucntion
Qx = np.zeros((6,6),float)
Qx[5,5] = 1
Qu = np.eye(4)
Qu = Qu*0.01

# calculating steps
Step = (N_data - N_data%C_horizon)/C_horizon
print(Step) 

d_min = 0
d_max = Step

dt = 1
d_star = 5

t = 0  # Last known timestep
M = 3  # Control horizon (must be less than P and at least 1)
P = 5  # Prediction horizon

x_sol = np.empty((6,N_data))
u_sol = np.empty((4,N_data))
d_sol = np.empty(N_data)

x_sol[:,0] = np.array([0., 0., 0., 0., 0., 0.])
d_sol[0] = 0

for it in range( ( (N_data - (N_data%M)) / M ) + 1 ):
	x = Variable(6,P)
	u = Bool(P)
	d = Variable(P)
	# z = Variable(3,P)
	z = Int(4,P)

	states = []
	for p in range(P-1):
		constraints =[
			x[:,p+1] == x[:,p] + dt*(A*x[:,p] + B*z[:,p]),
			x_min <= x[:,p+1], x_max >= x[:,p+1],
			u_min <= z[:,p+1], u_max >= z[:,p+1],
		] 
		cost = x[5,p+1] - x_des[5]
		states.append(Problem(Minimize(cost),constraints))

	prob = sum(states)
	prob.constraints += [x[:,0] == x_sol[:,t], z[:,0] == u_sol[:,t]]

	if it==0:		# initial condition for optimation
	 	prob.constraints += [x[:,0] == 0, z[:,0] ==0 ]

	if it>10:
		prob.constraints += [z[0,p]==5]

	else:
		prob.constraints += [z[0,p]==0]

	prob.solve()

	print prob.status
	# print x.value
	# print u.value
	# print d.value
	# print z.value

	# Set solution variables for M steps after t
	M = min(N_data-t-1,M)
	x_sol[:,t:t+M+1] = x.value[:,:M+1]
	u_sol[:,t:t+M+1] = z.value[:,:M+1]
	d_sol[  t:t+M+1] = d.value[  :M+1].flatten()
	t = t + M

# plot input data of Model Predictive Controller
plt.figure(3)
plt.subplot(411)
plt.plot(u_sol[0,:])
plt.xlabel('time')
plt.ylabel('xi1')
plt.grid(True)

plt.subplot(412)
plt.plot(u_sol[1,:])
plt.xlabel('time')
plt.ylabel('xi2')
plt.grid(True)

plt.subplot(413)
plt.plot(u_sol[2,:])
plt.xlabel('time')
plt.ylabel('xi3')
plt.grid(True)

plt.subplot(414)
plt.plot(u_sol[3,:])
plt.xlabel('time')
plt.ylabel('xi4')
plt.grid(True)

# plot states controlled by the solutions
plt.figure(4)
plt.subplot(221)
plt.plot(x_sol[2,:])
plt.xlabel('time')
plt.ylabel('eta3')
plt.grid(True)

plt.subplot(222)
plt.plot(x_sol[3,:])
plt.xlabel('time')
plt.ylabel('eta4')
plt.grid(True)

plt.subplot(223)
plt.plot(x_sol[4,:])
plt.xlabel('time')
plt.ylabel('eta5')
plt.grid(True)

plt.subplot(224)
plt.plot(x_sol[5,:])
plt.xlabel('time')
plt.ylabel('eta6')
plt.grid(True)

plt.figure(5)
plt.plot(d_sol)
plt.xlabel('time')
plt.ylabel('d')
plt.grid(True)
plt.show()

# begin to iterate the loop
# for k in range(0,Step-1):
# 	states = []

# 	x = Variable(6,P_horizon)
# 	u = Variable(3,P_horizon)
# 	u1_int = Bool(P_horizon)
# 	u2_int = Bool(P_horizon)
# 	u3_int = Bool(P_horizon)
# 	d = Variable(3,P_horizon)

# 	x_des[5] = 10

	# prediction part (optimization)
	# for p in range(0,P_horizon-1):
	# 	cost = square(x[5,p]-x_des[5]) + sum_squares(u[:,p])	# cost function 
	# 	constraints = [x[:,p+1] == A*x[:,p] + B*u[:,p],			# system description
		 			# inequality constraints
		 			# d[0,p+1] >= u1_int[p+1]*(d_min+dt),
		 			# d[1,p+1] >= u2_int[p+1]*(d_min+dt),
		 			# d[2,p+1] >= u3_int[p+1]*(d_min+dt),
		 			# d[0,p+1] <= u1_int[p+1]*(d_max+dt),
		 			# d[1,p+1] <= u2_int[p+1]*(d_max+dt),
		 			# d[2,p+1] <= u3_int[p+1]*(d_max+dt),
		 			# (d[0,p]+dt)-d[0,p+1] <= (1-u1_int[p+1])*(d_max-d_min),
		 			# (d[1,p]+dt)-d[1,p+1] <= (1-u2_int[p+1])*(d_max-d_min),
		 			# (d[2,p]+dt)-d[2,p+1] <= (1-u3_int[p+1])*(d_max-d_min),
		 			# (d[0,p]+dt)-d[0,p+1] >= (1-u1_int[p+1])*(d_min-d_max),
		 			# (d[1,p]+dt)-d[1,p+1] >= (1-u2_int[p+1])*(d_min-d_max),
		 			# (d[2,p]+dt)-d[2,p+1] >= (1-u3_int[p+1])*(d_min-d_max),
		 			# # inequality constraints
		 			# u[0,p+1] >= u1_int[p+1]*(d_min-ds),
		 			# u[1,p+1] >= u2_int[p+1]*(d_min-ds),
		 			# u[2,p+1] >= u3_int[p+1]*(d_min-ds),
		 			# u[0,p+1] <= u1_int[p+1]*(d_max-ds),
		 			# u[1,p+1] <= u2_int[p+1]*(d_max-ds),
		 			# u[2,p+1] <= u3_int[p+1]*(d_max-ds),
		 			# (d[0,p+1]-ds)-u[0,p+1] <= (1-u1_int[p+1])*(d_max-d_min),
		 			# (d[1,p+1]-ds)-u[1,p+1] <= (1-u2_int[p+1])*(d_max-d_min),
		 			# (d[2,p+1]-ds)-u[2,p+1] <= (1-u3_int[p+1])*(d_max-d_min),
		 			# (d[0,p+1]-ds)-u[0,p+1] >= (1-u1_int[p+1])*(d_min-d_max),
		 			# (d[1,p+1]-ds)-u[1,p+1] >= (1-u2_int[p+1])*(d_min-d_max),
		 			# (d[2,p+1]-ds)-u[2,p+1] >= (1-u3_int[p+1])*(d_min-d_max),
	# 	 			x_min <= x[:,p],							# state boundary (1)					
	# 	 			x[:,p] <= x_max,
	# 	 			u_min <= u[:,p],
	# 	 			u[:,p] <= u_max,
	# 	 			u[:,p+1]-u[:,p] <= du_max,
	# 	 			du_min <= u[:,p+1]-u[:,p]]							
	# 	states.append(Problem(Minimize(cost), constraints))		# merging problem and constraints

	# print k	
	# prob = sum(states)
	# if k==0:		# initial condition for optimation
	# 	prob.constraints += [x[:,0] == np.zeros((6,1),float),
	# 						x[:,1] - x[:,0] >= np.zeros((6,1),float),
	# 						u[:,0] == np.zeros((3,1),float)]


	# if k>0:			# condition for continuous iteration
	# 	prob.constraints += [x[:,0] == x_res[:,C_horizon*(k)-1],
	# 						 u[:,0] == u_res[:,C_horizon*(k)-1] ]

	# prob.solve()	# solve problem 
	# print prob.status	# check status of problem and solution 

	# # save and link the result data 
	# x_res[:,C_horizon*k:C_horizon*(k+1)] = x.value[:,0:C_horizon]
	# u_res[:,C_horizon*k:C_horizon*(k+1)] = u.value[:,0:C_horizon]

# # plot input data of Model Predictive Controller
# plt.figure(3)
# plt.subplot(311)
# plt.plot(u_res[0,:])
# plt.xlabel('time')
# plt.ylabel('xi1')
# plt.grid(True)

# plt.subplot(312)
# plt.plot(u_res[1,:])
# plt.xlabel('time')
# plt.ylabel('xi2')
# plt.grid(True)

# plt.subplot(313)
# plt.plot(u_res[2,:])
# plt.xlabel('time')
# plt.ylabel('xi3')
# plt.grid(True)

# # plot states controlled by the solutions
# plt.figure(4)
# plt.subplot(221)
# plt.plot(x_res[2,:])
# plt.xlabel('time')
# plt.ylabel('eta3')
# plt.grid(True)

# plt.subplot(222)
# plt.plot(x_res[3,:])
# plt.xlabel('time')
# plt.ylabel('eta4')
# plt.grid(True)

# plt.subplot(223)
# plt.plot(x_res[4,:])
# plt.xlabel('time')
# plt.ylabel('eta5')
# plt.grid(True)

# plt.subplot(224)
# plt.plot(x_res[5,:])
# plt.xlabel('time')
# plt.ylabel('eta6')
# plt.grid(True)
# plt.show()
