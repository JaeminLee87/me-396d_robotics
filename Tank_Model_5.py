from cvxpy import *
import numpy as np


####################
###   Constants  ###
####################

# Time constants
total_steps = 20
dt = 1.
tau = np.transpose([[3.,10.]]) # Must be positive and greater than dt

# Cost function parameters
x_goal = np.transpose([[100., -20.]])
Qx = np.eye(2)

# Awkward duration constant
d_star = 10

# Min and max of state variables
x_min = np.transpose([[-100., -100.]])
x_max = np.transpose([[ 100.,  100.]])
d_min = 0
d_max = total_steps

# Define A
alpha21 = -0.9
alpha12 = -0.2
A=np.array([[   -1   , alpha21 ],
			[ alpha12,    -1   ]])

# Define B
B=np.transpose([[-4., 5.]])

# Distribute tau to A and B
for i in range(tau.size):
	A[i,:]  = A[i,:]/tau[i,0]
	B[i,:] = B[i,:]/tau[i,0]

# Storage for solution
x_sol = np.empty((2,total_steps))
u_sol = np.empty(total_steps)
d_sol = np.empty(total_steps)


####################
### Optimization ###
####################
t = 0  # Last known timestep
M = 5  # Control horizon (must be less than P and at least 1)
P = 10 # Prediction horizon

# Initial Conditions
x_sol[:,0] = np.array([0.  , 0.])
d_sol[0] = 0

for it in range( ( (total_steps - (total_steps%M)) / M ) + 1 ):
	# Continuous variables
	x = Variable(2,P)
	u = Bool(P)
	d = Variable(P)
	z = Variable(P)

	states = []
	for p in range(P-1):
		constraints =[
			# State equation constraint
			x[:,p+1] == x[:,p] + dt*(A*x[:,p] + B*z[p]),

			# Inequality constraints for d[p+1]=u[p+1]*(d[p]+dt)
			d[p+1]>=u[p+1]*(d_min+dt),
			d[p+1]<=u[p+1]*(d_max+dt),
			(d[p]+dt)-d[p+1]<=(1-u[p+1])*(d_max-d_min),
			(d[p]+dt)-d[p+1]>=(1-u[p+1])*(d_min-d_max),

			# Inequality constraints for z[p+1]=u[p+1]*(d[p+1]-d_star)
			z[p+1]>=u[p+1]*(d_min-d_star),
			z[p+1]<=u[p+1]*(d_max-d_star),
			(d[p+1]-d_star)-z[p+1]<=(1-u[p+1])*(d_max-d_min),
			(d[p+1]-d_star)-z[p+1]>=(1-u[p+1])*(d_min-d_max),

			# Min and max x values
			x_min <= x[:,p+1],	x[:,p+1] <= x_max,
		]

		cost = quad_form(x[:,p+1]-x_goal,Qx)
		states.append(Problem(Minimize(cost),constraints))

	prob = sum(states)
	prob.constraints += [x[:,0] == x_sol[:,t], d[0] == d_sol[t],
					# IEC set 1 applied to p = 0
					d[0]>=u[0]*(d_min+dt), d[0]<=u[0]*(d_max+dt),
					# IEC set 2 applied to p = 0
					z[0]>=u[0]*(d_min-d_star),
					z[0]<=u[0]*(d_max-d_star),
					(d[0]-d_star)-z[0]<=(1-u[0])*(d_max-d_min),
					(d[0]-d_star)-z[0]>=(1-u[0])*(d_min-d_max)]

	prob.solve()

	print prob.status
	# print x.value
	# print u.value
	# print d.value
	# print z.value

	# Set solution variables for M steps after t
	M = min(total_steps-t-1,M)
	x_sol[:,t:t+M+1] = x.value[:,:M+1]
	u_sol[  t:t+M+1] = u.value[  :M+1].flatten()
	d_sol[  t:t+M+1] = d.value[  :M+1].flatten()
	t = t + M


import matplotlib.pyplot as plt

print d_sol.size
t = range(d_sol.size)

f = plt.figure()

# Plot (u_t)_1.
ax = f.add_subplot(411)
plt.step(t,u_sol,where='post')
plt.ylabel(r"$u(t)$", fontsize=16)
plt.yticks(np.linspace(-1.0, 1.0, 3))
plt.ylim([-0.2, 1.2])
plt.xticks([])

# Plot (d_t)_2.
plt.subplot(4,1,2)
plt.step(t,d_sol,where='post')
plt.ylabel(r"$d(t)$", fontsize=16)
# plt.yticks(np.linspace(-1, 1, 3))
plt.xticks([])

# Plot (x_t)_1.
plt.subplot(4,1,3)
x1 = x_sol[0,:]
plt.step(t,x1,where='post')
plt.ylabel(r"$x_1(t)$", fontsize=16)
# plt.yticks([-10, 0, 10])
plt.ylim([x_min[0], x_max[0]])
plt.xticks([])

# Plot (x_t)_2.
plt.subplot(4,1,4)
x2 = x_sol[1,:]
plt.step(t,x2,where='post')
# plt.yticks([-25, 0, 25])
plt.ylim([x_min[1], x_max[1]])
plt.ylabel(r"$x_2(t)$", fontsize=16)
plt.xlabel(r"$t$", fontsize=16)
plt.tight_layout()
plt.show()
