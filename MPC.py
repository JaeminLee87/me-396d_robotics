import numpy as np
import matplotlib.pyplot as plt
from cvxpy import *

vtau1, vtau2, vtau3, vtau4, vtau5, vtau6 = 2, 2, 2, 2, 2, 3
vgamma11, vgamma22, vgamma33 = 3, 1, 2
vbeta62, vbeta32, vbeta63, vbeta14, vbeta24, vbeta15, vbeta25, vbeta35, vbeta46, vbeta56 = -0.1, 0.1, 0.3, 0.4, -0.35, -0.15, 0.1, -0.1, 0.56, 0.2

# define the number of data (days)
N_data = 100
time = range(N_data)

# define delta t (1day)
unit_tau = 1

# define all zeta are zero
no_disturb = 0

## start Model Predictive Controller
## We are using cvxpy, thus, please make sure to install cvxpy.

# define state variable & input & output
A = np.zeros((6,6),float)
B = np.zeros((6,3),float)
C = np.eye(6)

# Transformation of system description in form of Matrix
A[0,0] = 1 - 1/float(vtau1)
A[1,1] = 1 - 1/float(vtau2)
A[1,2] = float(vbeta32)/float(vtau2)
A[1,5] = float(vbeta62)/float(vtau2)
A[2,2] = 1 - 1/float(vtau3)
A[2,5] = float(vbeta63)/float(vtau3)
A[3,0] = float(vbeta14)/float(vtau4) 
A[3,1] = float(vbeta24)/float(vtau4)
A[3,3] = 1 - 1/float(vtau4)
A[4,0] = float(vbeta15)/float(vtau5)
A[4,1] = float(vbeta25)/float(vtau5)
A[4,2] = float(vbeta35)/float(vtau5)
A[4,4] = 1 - 1/float(vtau5)
A[5,3] = float(vbeta46)/float(vtau6)
A[5,4] = float(vbeta56)/float(vtau6)
A[5,5] = 1 - 1/float(vtau6)

B[0,0] = float(vgamma11)/float(vtau1)
B[1,1] = float(vgamma22)/float(vtau2)
B[2,2] = float(vgamma33)/float(vtau3)

#x = Variable(6,N_data)
x_res = np.zeros((6,N_data),float)		# state results
x_des = np.array([0 ,0, 0, 0, 0, 10])	# desired state 

# constriants for states
x_min = np.array([0, -100, 0, 0, -100, 0])
x_max = np.array([1000, 1000, 1000, 1000, 500, 1000])

u_res = np.zeros((3,N_data),float)		# input results
u_des = np.zeros((3, N_data+1), float)	# desired input 

# constriants for inputs
u_min = np.array([0, 0, 0])			
u_max = np.array([100, 100, 100])
du_min = np.array([-10,-10,-10])
du_max = np.array([10, 10, 10])

# horizon steps for prediction and control
P_horizon = 10 			# horizon for predection
C_horizon = 5 			# horizon for control

# weighting matrix for cost fucntion
Qx = np.zeros((6,6),float)
Qx[5,5] = 1
Qu = np.eye(3)

# calculating steps
Step = (N_data - N_data%C_horizon)/C_horizon
print(Step) 

d_min = 0
d_max = Step

dt = 1
ds = 5


x = Variable(6,P_horizon)
u = Variable(3,P_horizon)
u1_int = Bool(P_horizon)
u2_int = Bool(P_horizon)
u3_int = Bool(P_horizon)
u_int = np.array([u1_int, u2_int, u3_int])
d = Variable(3,P_horizon)

x_des[5] = 10

print u_int