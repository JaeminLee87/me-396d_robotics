
import numpy as np

## define a class for SCT
import SCT_model

## define coefficients [tau, gamma, beta] 
# speed -> operating (vbeta32) : negative
# collision -> sucess rate (vbeta24) : negative
# experience -> operating time (vbeta15) : negative
# operating time -> trust (vbeta56) : negative 
# trust of robot -> collision rate (vbeta62) : negative

vtau1, vtau2, vtau3, vtau4, vtau5, vtau6 = 2, 2, 2, 2, 1, 3
vgamma11, vgamma22, vgamma33 = 1, 1, 1
vbeta62, vbeta32, vbeta63, vbeta14, vbeta24, vbeta15, vbeta25, vbeta35, vbeta46, vbeta56 = -0.1, 0.1, 0.3, 0.4, -0.35, -0.15, 0.1, -0.1, 0.56, 0.2

# define the number of data (days)
N_data = 100
time = range(N_data)

# define the state eta (1~6)
eta1 = range(N_data)
eta2 = range(N_data)
eta3 = range(N_data)
eta4 = range(N_data)
eta5 = range(N_data)
eta6 = range(N_data)

# define the initial state eta 
eta1[0] = 0
eta2[0] = 0
eta3[0] = 0
eta4[0] = 0
eta5[0] = 0
eta6[0] = 0

# define delta t (1day)
unit_tau = 1

# define all zeta are zero
no_disturb = 0
 
# define the inputs zai (1~8)
zai1 = range(N_data)
zai2 = range(N_data)
zai3 = range(N_data)

## define inputs
for i in range(0,N_data):
	# define input zai2
	if i < 14:
		zai2[i] = 0
	elif i < 20:
		zai2[i] = 5
	elif i < 26:
		zai2[i] = 0
	elif i < 30:
		zai2[i] = 5
	elif i < 35:
		zai2[i] = 0
	elif i < 40:
		zai2[i] = 5
	elif i < 45:
		zai2[i] = 0
	else:
		zai2[i] = 0

	# define input zai1	
	zai1[i] = i
	if zai1[i] > 10:
		zai1[i] = 10

	# define input zai (except 4 and 8)
	zai3[i] = 2

## Check System Model
# define Class SCT
Social_model01 = SCT_model.SCT()

# execute mdoel for SCT
for i in range(1,N_data):
	eta1[i] = Social_model01.experience(vgamma11, zai1[i-1], no_disturb, vtau1, eta1[i-1], i )
	eta2[i] = Social_model01.collision_rate(vgamma22, zai2[i-1], vbeta62, eta6[i-1], vbeta32, eta3[i-1], no_disturb, vtau2, eta2[i-1], i)
	eta3[i] = Social_model01.speed_robot(vgamma33, zai3[i-1], vbeta63, eta6[i-1], no_disturb, vtau3, eta3[i-1], i)
	eta4[i] = Social_model01.success_rate(vbeta14, eta1[i-1], vbeta24, eta2[i-1], no_disturb, vtau4, eta4[i-1], i)
	eta5[i] = Social_model01.operating_time(vbeta15, eta1[i-1], vbeta25, eta2[i-1], vbeta35, eta3[i-1], no_disturb, vtau5, eta5[i-1], i)
	eta6[i] = Social_model01.trust_robot(vbeta46, eta4[i-1], vbeta56, eta5[i-1], no_disturb, vtau6, eta6[i-1], i)	

# plotting data: inputs and states
import matplotlib.pyplot as plt
# figure 1: input data for validating the model 
plt.figure(1)
plt.subplot(131)
plt.plot(zai1)
plt.xlabel('time')
plt.ylabel('xi1')
plt.ylim([0, 12])
plt.grid(True)

plt.subplot(132)
plt.plot(zai2)
plt.xlabel('time')
plt.ylabel('xi2')
plt.ylim([0,6])
plt.grid(True)

plt.subplot(133)
plt.plot(zai3)
plt.xlabel('time')
plt.ylabel('xi3')
plt.grid(True)

# figure 2: states for validating the model
plt.figure(2)
plt.subplot(221)
plt.plot(eta3)
plt.xlabel('time')
plt.ylabel('eta3')
plt.grid(True)

plt.subplot(222)
plt.plot(eta4)
plt.xlabel('time')
plt.ylabel('eta4')
plt.grid(True)

plt.subplot(223)
plt.plot(eta5)
plt.xlabel('time')
plt.ylabel('eta5')
plt.grid(True)

plt.subplot(224)
plt.plot(eta6)
plt.xlabel('time')
plt.ylabel('eta6')
plt.grid(True)