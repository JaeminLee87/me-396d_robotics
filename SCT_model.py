class SCT:
## this class contians six tanks for the modelling: each explaination in UTwiki	confluence
## gamma: coefficients for inputs 
## beta: coefficients for states
## tau: time constant
## ()_previous: previous value
## zeta: disturbance
## t: time(day)

	def experience(self, gamma11, zai1, zeta1, tau1, eta1_prev, t):
		eta1 = (gamma11*zai1 - (1-tau1)*eta1_prev + zeta1)
		eta1 = eta1/tau1
		return eta1

	def collision_rate(self, gamma22, zai2, beta62, eta6, beta32, eta3, zeta2, tau2, eta2_prev, t):
		eta2 = (gamma22*zai2 + beta62*eta6 + beta32*eta3 - (1-tau2)*eta2_prev + zeta2) 
		eta2 = eta2/tau2
		return eta2

	def speed_robot(self, gamma33, zai3, beta63, eta6, zeta3, tau3, eta3_prev, t):
		eta3 = (gamma33*zai3 + beta63*eta6 - (1-tau3)*eta3_prev + zeta3)
		eta3 = eta3/tau3
		return eta3

	def success_rate(self, beta14, eta1, beta24, eta2, zeta4, tau4, eta4_prev, t):
		eta4 = (beta14*eta1 + beta24*eta2 - (1-tau4)*eta4_prev + zeta4)
		eta4 = eta4/tau4
		return eta4

	def operating_time(self, beta15, eta1, beta25, eta2, beta35, eta3, zeta5, tau5, eta5_prev, t):
		eta5 = (beta15*eta1 + beta25*eta2 + beta35*eta3 - (1-tau5)*eta5_prev + zeta5)
		eta5 = eta5/tau5
		return eta5

	def trust_robot(self, beta46, eta4, beta56, eta5, zeta6, tau6, eta6_prev, t):
		eta6 = (beta46*eta4 + beta56*eta5 - (1-tau6)*eta6_prev + zeta6)
		eta6 = eta6/tau6
		return eta6