% This is the base code that makes reference to a number of subfunctions 
clear; clc;
m = 1; % mass
l = 0.5; % length
b = 0.1; % damping 
I = m*l^2; % moment of inertia 
g = 9.8; % gravity

nTime = 21; % discretize time into 20 sections 

load xu_trajectory.mat

nsample=30;
step=round(2000/nsample);
X2=zeros(1,nsample);
U2=zeros(1,nsample);
for z=1:nsample
    idx=(z-1)*step+1;
   X2(1,z)=xo_t(1,idx);
   X2(2,z)=xo_t(2,idx);
   U2(z)=uo_t(idx);
 end

tSol = linspace(0,3,nsample);

xSol = X2(1,:); % theta
ySol = X2(2,:); % theta_dot
uSol = U2; % input torque values
% considering the optimal time of 1.9725 s
tSpan = [0,3];

% order of the polynomial we used to fit the data 
nFit =9;  

% polynomial curve fitting 
xFit = polyfit(tSol,xSol,nFit);
yFit = polyfit(tSol,ySol,nFit);
uFit = polyfit(tSol,uSol,nFit);

Q = diag([10, 1]);  % Q matrix as provided in the paper  
R = 20;       % R as defined in the paper 
F = [ 230.8806   49.2538;
   49.2538   10.6640];  % final Q value Q_f, came from calling lqr 
tol = 1e-6;  % when we use ODE45, this is the error tolerance 

% information for the for loop below
linSys = @(t)getLinTraj(t,xFit,yFit,uFit,m,g,l,I,b);
Soln = trajectoryLqr(tSol,linSys,Q,R,F,tol); % calculate S, K, A, B, and 
                                             % S_dot by calling trajectory
                                             % LQR
nSoln = length(tSol);
% for loop is used to draw regions of attraction around our 20 discretrized
% points along the trajectory
% We are computing rho's without any constraint
for idx= 1: nSoln
    idx;
    temp_A = Soln(idx).A;
    temp_B = Soln(idx).B;
    temp_S = Soln(idx).S;
    temp_S_dot = Soln(idx).S_dot;
    temp_K = Soln(idx).K;
    x1_goal = polyval(xFit,tSol(idx));
    x2_goal = polyval(yFit,tSol(idx));
    u_goal = polyval(uFit,tSol(idx));
    rho(idx) = getROA_ti(temp_A, temp_B, temp_K, temp_S, temp_S_dot, m, g, l, b, I, u_goal, x1_goal, x2_goal);
end

% this for loop is to place the constraint that the previous rho is less
% than or equal to the next rho as we move along the trajectory from the
% starting point to the goal state
prev_rho = rho(nSoln);
for idx= 1:nSoln
    i = nSoln-idx+1;
    temp_S = Soln(i).S;
    x1_goal = polyval(xFit,tSol(i));
    x2_goal = polyval(yFit,tSol(i));
    
    temp_rho(i) = prev_rho;
%     
%     if rho(i) <= prev_rho
%         temp_rho(i) = rho(i);
%     end
    getROA_tv(temp_S, x1_goal, x2_goal, temp_rho(i));
    prev_rho = temp_rho(i);
    xlim([-2 5]);
    ylim([-8 8]);
end

% used to store information for theta and theta_dot
x1_goal_set = [];
x2_goal_set = [];

% compiles information to plot the 2D region of attraction along the trajectory at the 20
% discretized points 
for idx= 1:nSoln
    i = nSoln-idx+1;
    temp_S = Soln(i).S;
    x1_goal = polyval(xFit,tSol(i));
    x1_goal_set = [x1_goal_set x1_goal];
    x2_goal = polyval(yFit,tSol(i));
    x2_goal_set = [x2_goal_set x2_goal];
    [sum.sample sum.h] = getROA_tv(temp_S, x1_goal, x2_goal, temp_rho(i));
    tube(i, 1) = sum;
end

% plot region of attraction
hold on;
plot(x1_goal_set,x2_goal_set,'k-','LineWidth',1);

