function traj = simulatePendlum(init,param)

u0 = init.torque;
nGrid = param.nGrid;

% Set up initial conditions for ode45
x0 = 0;  y0 = 0;  %Start at the origin


% Set up arguments for ode45
userFun = @(t,z)PendlumDynamics(t,z);  %Dynamics function
tSpan = [0,10];  %Never plan on reaching final time
z0 = [x0;y0];  %Initial condition
options = odeset('Events',@groundEvent,'Vectorized','on');

% f0 = @(t,x,u) [-x(2) ; x(1) + x(2)*(x(1)^2-1)]; % Populate this with your dynamics.


% Run a simulation
sol = ode45(userFun, tSpan, z0, options);

% Extract the solution on uniform grid:
traj.t = linspace(sol.x(1), sol.x(end), nGrid);
z = deval(sol,traj.t);
traj.x = z(1,:); 
traj.y = z(2,:); 



end