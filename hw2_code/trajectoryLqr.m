% The purpose of this function is to return the S, K, A, B, and S_dot
% matrices along a trajectory at a specific time for our time-varying LQR
% problem 

% Soln contains the described matrices 

% this code is drawn from the following link:
% https://www.mathworks.com/matlabcentral/fileexchange/54432-continuous-time--finite-horizon-lqr
function Soln = trajectoryLqr(t,linSys,Q,R,F,tol)

% number of states
nState = size(Q,1);
% number of inputs 
nInput = size(R,1);

% declare the function to compute S_dot
userFun = @(t,z)rhs(t,z,linSys,Q,R,nState);

% Qf
z0 = reshape(F,nState*nState,1);
% backward time span (working backwards to solve for S over time) 
tSpan = [t(end),t(1)];

options = odeset();
options.RelTol = tol;
options.AbsTol = tol;
% use ode45 to solve for S
sol = ode45(userFun,tSpan,z0);
z = deval(sol,t);

% declare the solution variable
nSoln = length(t);

% dectlare variables to store S, K, A, B, and S_dot
Soln(nSoln).t = 0;
Soln(nSoln).K = zeros(nState,nInput);
Soln(nSoln).A = zeros(nState,nState);
Soln(nSoln).B = zeros(nState,nInput);
Soln(nSoln).S = zeros(nState,nState);
Soln(nSoln).S_dot = zeros(nState,nState);
Soln(nSoln).E = zeros(nState,1);

% for loop to store S, K, A, B, and S_dot values 
for idx=1:nSoln
    i = nSoln-idx+1;
    zNow = z(:,i);
    tNow = t(i);
    S = reshape(zNow,nState,nState);
    [A,B] = linSys(tNow);
    K = R\(B'*S);
    S_dot = S;
    S_dot = ricatti(A,B,Q,R,S_dot);
    Soln(i).t = tNow;
    Soln(i).K = K;
    Soln(i).S = S;
    Soln(i).S_dot = S_dot;
    Soln(i).A = A;
    Soln(i).B = B;
    Soln(i).E = eig(A-B*K);
end

end

% calculation of S_dot at a particular time. This function calls ricatti to
% generate A and B
function dz = rhs(t,z,linSys,Q,R,nState)
P = reshape(z,nState,nState);
[A,B] = linSys(t); % generate A and B
dP = ricatti(A,B,Q,R,P);
dz = reshape(dP,nState*nState,1);
end
% calculation of S_dot
function [dP, K] = ricatti(A,B,Q,R,P)
K = R\B'*P;
dP = -(A'*P + P*A - P*B*K + Q);    %S_dot
end