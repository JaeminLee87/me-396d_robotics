clc; clear;

%% Parameter setting for simple pendulum model
m = 1; 
l = 0.5; 
b = 0.1;
g = 9.8;
I = m*l*l;

%% Goal state
x_s = [0; 0];
x_g = [pi; 0];

%% Initialize for LQR parameters
Q = diag([10;1]);
R = 20;
%% Initialize for time-invariant LQR parameters
A = [0, 1;
    m*g*l/I, -b/I];
B = [0; 
    1/I];


%% Solve LQR (time-invariant)
[K, S, E] = lqr(A,B,Q,R);

u_max = 3; % 3Nm
dt = 0.001;

%% Time, state, Control Input 
t = [0:dt:3];
x_t = zeros(2, size(t,2));  
u_t = zeros(1, size(t,2));

ddth_t = zeros(1, size(t,2));

x_t(:, 1) = x_s; 

tneg = 0.4;
tpos= 1;

for i=2:size(t,2)
    th = x_t(1, i-1);
    dth = x_t(2, i-1);
    x_bar = x_t(:, i-1) - x_g;

    u= -K*x_bar;
    
    
    if (t(i) < tneg)
        u = -u_max;
    end
    if (t(i) >= tneg && t(i) < tpos)
        u = 10*t(i) - 7;
    end
    
     if (u > u_max)
         u = u_max;
     end
     if (u < -u_max)
         u = -u_max;
     end
    
%      x_bardot=A*x_bar+B*u;
%      x_bar = x_bar + xbardot*dt;
     
   
    ddth = (1/I)*(-b*dth - m*g*l*sin(th) + u);
    
    dth_next = ddth*dt + dth;
    th_next = dth*dt + th;

     x_t(:, i) = [th_next; dth_next];
    u_t(:, i) = u;
    ddth_t(:, i) = ddth;
end

figure(1)
subplot(2,1,1)
plot(x_t(1, :), x_t(2,:),'Color','k','LineWidth',1.5)
hold on
plot(0,0,'r*','MarkerSize',2)
hold on
text(0,1,'S')
hold on
text(3.14,1,'G')
plot(3.14,0,'r*','MarkerSize',2)
xlabel('x1(theta)');
ylabel('x2(theta dot)');

subplot(2,1,2)
plot(t(2:end), u_t(2:end),'Color','k','LineWidth',1.5)
hold on
plot(t, 3*ones(1, size(t,2)),'--r');
hold on
plot(t, -3*ones(1, size(t,2)),'--r');
xlabel('time')
ylabel('Torque')

figure();
subplot(2,1,1)
plot(t, x_t(1,:),'Color','k','LineWidth',1.5)
hold on
plot(t, x_g(1)*ones(1, size(t,2)),'Color','r');
xlabel('time')
ylabel('x1(t)')

subplot(2,1,2)
plot(t, x_t(2,:),'Color','k','LineWidth',1.5)
hold on
plot(t, x_g(2)*ones(1, size(t,2)),'r');
xlabel('time')
ylabel('x2(t)')

xo_t = x_t;
uo_t = u_t;

save('xu_trajectory.mat', 'xo_t', 'uo_t')

