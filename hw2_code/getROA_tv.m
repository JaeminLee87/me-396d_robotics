function [rho] = getROA_tv(A, B, K, S, S_dot, m, g, l, b, I, u_goal, x1_goal, x2_goal)

% u = -Kx
% values within K matrix
k1 = K(1);
k2 = K(2);

% goal states for theta and theta_dot
x_goal =[x1_goal; x2_goal];
pvar x1_bar x2_bar;
% linearize around goal states 
x_bar = [x1_bar;x2_bar];

% f_cl Taylor expansion used for caluculating J_dot 
f = [x2_bar;
    (g*l*m*cos(x1_goal)*(x1_bar)^3)/(6*I) - (b/I + k2/I)*(x2_bar) - (x1_bar)*(k1/I + (g*l*m*cos(x1_goal))/I) - (g*l*m*cos(x1_goal)*(x1_bar)^5)/(120*I) + (g*l*m*sin(x1_goal)*(x1_bar)^2)/(2*I) - (g*l*m*sin(x1_goal)*(x1_bar)^4)/(24*I)
    ];

g1 = (x_bar)'*S_dot * (x_bar) + 2 * (x_bar)'*S*f; % J_dot for time varying
                                                  % system
  
g2 = (x_bar)'*S*(x_bar); % J_star 
g22 = (x_bar - x_goal)'*S*(x_bar - x_goal); % J_star with consideration of goal states

% generate h(x) (order 2) using lagragian multipliers 
z = monomials(x_bar,0:2);

% pcontain is used to determine our region of attraction
[gbnds,sopt] = pcontain(g1,g2, z);

rho = gbnds(1); % maximum rho

end