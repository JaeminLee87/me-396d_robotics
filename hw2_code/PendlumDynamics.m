function [dz, du] = PendlumDynamics(~,z)

% This function computes the dynamics of a cannon ball travelling through
% the air with quadratic drag
%
% INPUTS:
%   ~ = [1 x N] time vector (not used)
%   z = [2 x N] state vectory
%       z(1,:) = x = theta
%       z(2,:) = y = angular velocity 

%   u = input torque
%
% OUTPUTS:
%   dz = d(x)/dt = time-derivative of the state
%
% NOTES:
%   - Assume unit gravity and mass
%   - Assume that cannon ball is a point mass  (no rotational inertia)
%
% Pendulum mass
m = 1.0;
% Pendulum length
l = 0.5;
% Friction coefficient
b = 0.1;
g=9.8;

%%Goal points
xG = [pi;
      0.0];

%% Compute state matrices
% State equation A matrix
A = [0.0            , 1.0       ;
     -g/l*cos(xG(1)), -b/(m*l^2)];
% State equation B matrix
B = [0.0        ;
     1.0/(m*l^2)];

%% Define cost function parameters
% State cost matrix
Q = diag([10.0, 1.0]);
% Control cost matrix
R = 15.0;
% Cross cost matrix
N = [0.0;
     0.0];

 % LQR 
 [K,S,E] = lqr(A,B,Q,R,N);
 
   zbar=z-xG;
   ubar = -K*zbar;
   % Compute xdot
   dz = A*zbar + B*ubar;
 
% f2x1 = -g/l*cos(xG(1))-K(1)/(m*l^2);
% f2x2 = -b/(m*l^2)-K(2)/(m*l^2);
% f2x1_2 = g/l*sin(xG(1));
% f2x1_3 = g/l*cos(xG(1));

% dz = zeros(size(z));
%theta =theta1 (derivative of position states is velocity states)
% dz(1,:)=z(2,:);
% dz(2,:)= z(1,:)*f2x1+z(2,:)*f2x2 +z(1,:)^2*f2x1_2/2 +z(1,:)^3*f2x1_3/6;
% Compute the speed (used for drag force calculation)

end